<?php


namespace DpnielCh\UrlSigner\Tests;

use DateTime;
use DpnielCh\UrlSigner\Signing\Md5UrlSigner;
use DpnielCh\UrlSigner\UrlSigner;
use Symfony\Component\HttpKernel\Exception\HttpException;

class ValidateSignatureTest extends TestCase
{
    public function setUp() : void
    {
        parent::setUp();
    }
    /** @test */
    public function it_registered_an_md5_url_signer_in_the_container()
    {
        $instance = $this->app['url-signer'];
        self::assertInstanceOf(Md5UrlSigner::class, $instance);
        self::assertInstanceOf(UrlSigner::class, $instance);
    }

    /** @test */
    public function it_defaults_to_the_default_expiration_time_in_days()
    {
        $url = $this->app['url-signer']->sign("{$this->hostName}/protected-route");
        $defaultExpiration = config('url-signer.default_expiration_time_in_days');
        $expiresParameter = config('url-signer.parameters.expires');

        parse_str(parse_url($url)['query'], $query);

        $expectedExpiration = (new DateTime())->modify("{$defaultExpiration} days")->getTimestamp();

        // We can't test the exact timestamp since it's generated inside the URL signer.
        // Instead we check if it's in a 5 minute interval of the exptected result.
        self::assertLessThan($expectedExpiration + 60 * 5, $query[$expiresParameter]);
        self::assertGreaterThan($expectedExpiration - 60 * 5, $query[$expiresParameter]);
    }

    /** @test */
    public function it_rejects_an_unsigned_url()
    {
        $url = "{$this->hostName}/protected-route";

        $this->assert403Response($url);
    }

    /** @test */
    public function it_accepts_a_signed_url()
    {
        $url = $this->app['url-signer']->sign("{$this->hostName}/protected-route", 1);

        $this->assert200Response($url);
    }

    /** @test */
    public function it_rejects_a_forged_url()
    {
        $url = "{$this->hostName}/protected-route?expires=123&signature=456";

        $this->assert403Response($url);
    }

    /**
     * Assert wether a GET request to a URL returns a 200 response.
     *
     * @param string $url
     */
    protected function assert200Response($url)
    {
        self::assertEquals(200, $this->call('GET', $url)->getStatusCode());
    }

    /**
     * Assert wether a GET request to a URL returns a 403 response.
     *
     * @param string $url
     */
    protected function assert403Response($url)
    {
        // Laravel 5.3 doesn't seem to throw an HttpException in this context,
        // so we're going to do a check in the try and in the catch
        // (only one of them will get called)
        try {
            $response = $this->call('GET', $url);
            self::assertEquals(403, $response->getStatusCode());
            return;
        } catch (HttpException $e) {
            self::assertEquals(403, $e->getStatusCode());
            return;
        }

        $this->fail('Response was ok');
    }
}
