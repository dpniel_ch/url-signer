# URL Signing Package

## Installation

You can install the package via composer:

First add this repository to your composer

```json
{
    ....
    "repositories": [
        {
            "type": "vcs",
            "url": "https://bitbucket.org/dpniel_ch/url-signer.git"
        }
    ]
}
```

and then 

```bash
composer require dpniel_ch/url-signer
```

To enable the package, register the serviceprovider, and optionally register the facade:

```php
// config/app.php

'providers' => [
    ...
    DpnielCh\UrlSigner\UrlSignerServiceProvider::class,
];

'aliases' => [
    ...
    'UrlSigner' => DpnielCh\UrlSigner\UrlSignerFacade::class,
];
```

## Configuration

The configuration file can optionally be published via:

```
php artisan vendor:publish --provider="DpnielCh\UrlSigner\UrlSignerServiceProvider"
```

This is the contents of the file:

```php
return [

    /*
    * This string is used the to generate a signature. You should
    * keep this value secret.
    */
    'signatureKey' => config('app.key'),

    /*
     * The default expiration time of a URL in days.
     */
    'default_expiration_time_in_days' => 1,

    /*
     * These strings are used a parameter names in a signed url.
     */
    'parameters' => [
        'expires' => 'expires',
        'signature' => 'signature',
    ],

];
```

## Usage

### Signing URLs

URL's can be signed with the `sign`-method:
```php
UrlSigner::sign('https://myapp.com/protected-route');
```
By default the lifetime of an URL is one day. This value can be change in the config-file.
If you want a custom life time, you can specify the number of days the URL should be valid:

```php
//the generated URL will be valid for 5 days.
UrlSigner::sign('https://myapp.com/protected-route', 5);
```

For fine grained control, you may also pass a `DateTime` instance as the second parameter. The url
will be valid up to that moment. This example uses Carbon for convenience:
```php
//This URL will be valid up until 2 hours from the moment it was generated.
UrlSigner::sign('https://myapp.com/protected-route', Carbon\Carbon::now()->addHours(2); );
```

### Validating URLs
To validate a signed URL, simply call the `validate()`-method. This return a boolean.
```php
UrlSigner::validate('https://app.com/protected-route?expires=xxxxxx&signature=xxxxxx');
```

### Protecting routes with middleware
The package also provides a middleware to protect routes:

```php
Route::get('protected-route', ['middleware' => 'signedurl', function () {
    return 'Hello secret world!';
}]);
```
Your app will abort with a 403 status code if the route is called without a valid signature.nielCh\UrlSigner\UrlSignerServiceProvider::class

### Testing

``` bash
./vendor/bin/phpunit
```

### Security

If you discover any security related issues, please email danchapman1819@gmail.com instead of using the issue tracker.

## License

The MIT License (MIT). Please see [License File](LICENSE.md) for more information.
