<?php


namespace DpnielCh\UrlSigner\Signing;


use League\Url\UrlImmutable;

class Md5UrlSigner extends BaseUrlSigner
{
	/**
	 * @inheritDoc
	 */
	function createSignature($url, string $expiration)
	{
		return md5(sprintf("%s::%s::%s", (string)$url, $expiration, $this->signatureKey));
	}
}
