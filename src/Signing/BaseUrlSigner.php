<?php


namespace DpnielCh\UrlSigner\Signing;


use DateTime;
use DpnielCh\UrlSigner\Contracts\UrlSignerInterface;
use DpnielCh\UrlSigner\Exceptions\InvalidExpiration;
use DpnielCh\UrlSigner\Exceptions\InvalidSignatureKey;
use League\Uri\Http;
use League\Uri\QueryString;
use League\Url\UrlInterface;
use Psr\Http\Message\UriInterface;

abstract class BaseUrlSigner implements UrlSignerInterface
{
    /**
     * Secret key used to generate secure signatures
     *
     * @var string
     */
    protected $signatureKey;

    /**
     * The URL query param name for expiration value
     *
     * @var string
     */
    protected $expiresParam;

    /**
     * The URL query param for signature value
     *
     * @var string
     */
    protected $signatureParam;

    /**
     * BaseUrlSigner constructor.
     * @param string $signatureKey
     * @param string $signatureParam
     * @param string $expiresParam
     * @throws InvalidSignatureKey
     */
    public function __construct(string $signatureKey, string $signatureParam = "signature", string $expiresParam = "expires")
    {
        if ($signatureKey === '') {
            throw new InvalidSignatureKey("No signature key given");
        }
        $this->signatureKey = $signatureKey;
        $this->signatureParam = $signatureParam;
        $this->expiresParam = $expiresParam;
    }

    /**
     * @param UrlImmutable|string $url
     * @param string $expiration
     * @return string
     */
    abstract function createSignature($url, string $expiration);

    /**
     * @inheritDoc
     * @throws InvalidExpiration
     */
	public function sign(string $url, $expiration)
	{
		$uri = Http::createFromString($url);
        $expires = $this->getExpirationTimestamp($expiration);
        $signature = $this->createSignature((string)$uri, $expires);
        return (string)$this->signUrl($uri, $expires, $signature);
	}

	/**
	 * @inheritDoc
	 */
	public function validate(string $url)
	{
		$uri = Http::createFromString($url);

		$query = QueryString::extract($uri->getQuery());

		if (!$this->hasValidQueryParams($query)) {
		    return false;
        }

		if (!$this->isFuture($query[$this->expiresParam])) {
		    return false;
        }

		if (!$this->hasValidSignature($uri)) {
		    return false;
        }
		return true;
	}

    /**
     * Add expiration and signature query params to url
     *
     * @param UrlImmutable $url
     * @param string $expiration
     * @param string $signature
     *
     * @return UrlImmutable
     */
    protected function signUrl(UriInterface $url, string $expiration, string $signature)
    {
		$query = QueryString::extract($url->getQuery());

        $query[$this->expiresParam] = $expiration;
        $query[$this->signatureParam] = $signature;

        return $url->withQuery($this->buildQueryStringFromArray($query));
	}

    /**
     * @param DateTime|int $expiration
     * @return string
     * @throws InvalidExpiration
     */
	protected function getExpirationTimestamp($expiration)
    {
        if (is_int($expiration)) {
            $expiration = (new DateTime())->modify((int)$expiration . ' days');
        }

        if (!$expiration instanceof DateTime) {
            throw new InvalidExpiration("Expiration must be instance of DateTime or int");
        }

        if (!$this->isFuture($expiration->getTimestamp())) {
            throw new InvalidExpiration("Expiration date must be set in the future");
        }

        return (string)$expiration->getTimestamp();
    }

    /**
     * @param int $timestamp
     * @return bool
     */
    private function isFuture(int $timestamp)
    {
        return ((int) $timestamp) >= (new DateTime())->getTimestamp();
    }

    /**
     * @param QueryInterface $query
     * @return bool
     */
    private function hasValidQueryParams(array $query)
    {
        if (!isset($query[$this->expiresParam])) {
            return false;
        }

        if (!isset($query[$this->signatureParam])) {
            return false;
        }

        return true;
    }

    /**
     * @param UrlImmutable $url
     * @return bool
     */
    private function hasValidSignature(UriInterface $url)
    {
        $query = QueryString::extract($url->getQuery());

        $expiration = $query[$this->expiresParam];
        $providedSig = $query[$this->signatureParam];
        $intendedUrl = $this->getTargetUrl($url);

        $validSig = $this->createSignature($intendedUrl, $expiration);
        return hash_equals($validSig, $providedSig);
    }

    /**
     * @param UrlImmutable $url
     * @return UrlImmutable|UrlInterface
     */
    private function getTargetUrl(UriInterface $url)
    {
        $intendedQuery = QueryString::extract($url->getQuery());
        unset($intendedQuery[$this->expiresParam]);
        unset($intendedQuery[$this->signatureParam]);
        return $url->withQuery($this->buildQueryStringFromArray($intendedQuery));
    }

    protected function buildQueryStringFromArray(array $query)
    {
        $buildQuery = [];
        foreach ($query as $key => $value) {
            $buildQuery[] = [$key, $value];
        }

        return QueryString::build($buildQuery);
    }

}
