<?php

namespace DpnielCh\UrlSigner;

use Illuminate\Support\Facades\Facade;

/**
 * @see \DpnielCh\UrlSigner\Skeleton\SkeletonClass
 */
class UrlSignerFacade extends Facade
{
    /**
     * Get the registered name of the component.
     *
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return 'url-signer';
    }
}
