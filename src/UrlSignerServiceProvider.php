<?php

namespace DpnielCh\UrlSigner;

use DpnielCh\UrlSigner\Contracts\UrlSignerInterface;
use DpnielCh\UrlSigner\Middleware\ValidateSignature;
use Illuminate\Routing\Router;
use Illuminate\Support\ServiceProvider;

class UrlSignerServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     */
    public function boot()
    {
        if ($this->app->runningInConsole()) {
            $this->publishes([
                __DIR__ . '/../config/url-signer.php' => config_path('url-signer.php'),
            ], 'config');
        }
    }

    /**
     * Register the application services.
     */
    public function register()
    {
        // Automatically apply the package configuration
        $this->mergeConfigFrom(__DIR__ . '/../config/url-signer.php', 'url-signer');

        $config = config('url-signer');

        // Register the main class to use with the facade
        $this->app->singleton('url-signer', function () use ($config) {
            return new UrlSigner(
                $config['signatureKey'],
                $config['parameters']['signature'],
                $config['parameters']['expires']
            );
        });

        $this->app[Router::class]->aliasMiddleware('signedurl', ValidateSignature::class);
    }
}
