<?php


namespace DpnielCh\UrlSigner\Middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class ValidateSignature
{
    /**
     * Handle an incoming request.
     *
     * @param Request $request
     * @param Closure $next
     *
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        $urlIsSigned = app('url-signer')->validate($request->fullUrl());

        if (! $urlIsSigned) {
            Log::error("AUth sign failed: " . print_r([
                "key" => config("url-signer.signatureKey"),
                "full_url" => $request->fullUrl(),
                "is_signed" => $urlIsSigned
            ], true));
            abort(403);
        }

        return $next($request);
    }
}
