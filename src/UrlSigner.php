<?php

namespace DpnielCh\UrlSigner;

use DpnielCh\UrlSigner\Signing\Md5UrlSigner;

class UrlSigner extends Md5UrlSigner
{
    public function sign(string $url, $expiration = null)
    {
        return parent::sign($url, $expiration ?: config('url-signer.default_expiration_time_in_days'));
    }
}
