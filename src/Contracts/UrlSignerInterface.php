<?php


namespace DpnielCh\UrlSigner\Contracts;


use DateTime;

interface UrlSignerInterface
{
    /**
     * Generate a secure url for a route
     * @param string $url
     * @param DateTime|int $expiration
     * @return string
     */
    public function sign(string $url, $expiration);

    /**
     * Validate a signed url
     *
     * @param string $url
     * @return bool
     */
    public function validate(string $url);
}
